(defpackage :slipper
  :use :cl)

(require :sockets)

(in-package :slipper)

(defparameter *following-file* "~/.slipperrc")
(defparameter *twtxt-file* "/twtxt.txt")

(defun split (string char)
  (loop for i = 0 then (1+ j)
     as j = (position char string :start i)
     collect (subseq string i j)
     while j))

(defun join (list char)
  )

(defun connect-to (host port uri)
  "connects to a given socket and reads all lines"
  (let* ((address (sb-bsd-sockets:get-host-by-name host))
         (host (car (sb-bsd-sockets:host-ent-addresses address)))
         (socket (make-instance 'sb-bsd-sockets:inet-socket :type :stream :protocol :tcp)))

    (sb-bsd-sockets:socket-connect socket host port)

    (let ((stream (sb-bsd-sockets:socket-make-stream socket :input t :output t)))
      (format stream "~a~%" uri)
      (force-output stream)

      (loop for line = (read-line stream nil nil)
         while line
         collect line)
      )))

(defun read-twtxt-from (url)
  (let* ((splitted-url (split url "/"))
         (host (car splitted-url))
         (uri (join (cdr splitted-url) "/")))
    (princ (host uri))
    ))

(defun follow (user url)
  "adds a twtxt stream to your watch list"
  (format t "Following ~a on ~a" user url)
  (write-file *following-file*
              (lambda () (format t "~a~c~a~%" user #\tab url))))

(defun pager (urls)
  "shows all tweets sorted by date for the given urls"
  (loop for url = url
     while url
     collect (read-twtxt-from url)))

(defun sort (responses)
  "sorts responses by a given date"
  (princ "i'll sort all responses"))

(defun flatten (list)
  (reduce (lambda (l r) (if (listp r) (flatten r) (append l r))) list :initial-value '()))

(defun parse-twtxt (responses)
  (mapcar 'parse-twtxt-line responnses))

(defun parse-twtxt-line (line)
  (let ((cuted-line (split #\tab line)))
    (list (cons 'TIME (car cuted-line))
          (cons 'MSG (cadr cuted-line)))))

(defun write-file (filename fun)
  (with-open-file (*standard-output*
                   filename
                   :direction :output
                   :if-exists :append
                   :if-does-not-exist :create)
    (funcall fun)))

(defun create-date-stamp ()
  "generates the RFC 3339 date-time"
  (multiple-value-bind
        (second minute hour date month year day-of-week dst-p tz)
      (get-decoded-time)
    (format t "~D-~2,'0D-~2,'0DT~2,'0D:~2,'0D:~2,'0D"
            year
            month
            day-of-week
            hour
            minute
            second
            )))
  
(defun create-twtxt (txt)
  (format t "~A~C~A~%" (create-date-stamp) #\tab txt))

(defun main ()
  (write-file "~/test.txt" (lambda () (create-twtxt "hallo welt"))))
